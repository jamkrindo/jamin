package com.jamkrindo.jamin.addbusiness

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton
import com.jamkrindo.jamin.common.model.category.Category
import com.jamkrindo.jamin.databinding.ActivityAddBusinessBinding
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter
import com.schibstedspain.leku.*
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.session.Session
import com.jamkrindo.jamin.main.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*
import java.text.SimpleDateFormat


class AddBusinessActivity : AppCompatActivity() {

    private lateinit var viewModel: AddBusinessViewModel
    private lateinit var categorySpinner: MaterialSpinner
    private lateinit var typeSpinner: MaterialSpinner
    private lateinit var submitButton: CircularProgressButton

    private val session by lazy {
        Session(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(AddBusinessViewModel::class.java)
        val binding = DataBindingUtil.setContentView<ActivityAddBusinessBinding>(
            this, R.layout.activity_add_business
        )
        binding.lifecycleOwner = this
        binding.addBusinessViewModel = viewModel

        categorySpinner = findViewById(R.id.add_business_category_spinner)
        typeSpinner = findViewById(R.id.add_business_type_spinner)
        submitButton = findViewById(R.id.create_business)

        categorySpinner.setOnItemSelectedListener { _, _, _, item ->
            viewModel.selectedCategory.value = item as Category
        }

        typeSpinner.setOnItemSelectedListener { _, _, _, item ->
            viewModel.selectedType.value = item as String
        }

        viewModel.categories.observe(this, Observer {
            categorySpinner.setAdapter(MaterialSpinnerAdapter(this, it))
        })

        typeSpinner.setAdapter(MaterialSpinnerAdapter(this, listOf("INDIVIDUAL", "GROUP")))

        viewModel.pending.observe(this, Observer {
            if (it) {
                submitButton.startAnimation()
            } else {
                submitButton.revertAnimation()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        })

        GlobalScope.launch(Dispatchers.IO) {
            viewModel.loadAvailableCategories()
        }
    }

    fun showPlacePicker(view: View) {
        val locationPickerIntent = LocationPickerActivity.Builder()
            .withGeolocApiKey("AIzaSyDY937stLepU5xA0L8xsqgp39f_lfWI4Lw")
            .withDefaultLocaleSearchZone()
            .shouldReturnOkOnBackPressed()
            .build(applicationContext)

        startActivityForResult(locationPickerIntent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            val latitude = data?.getDoubleExtra(LATITUDE, 0.0)
            viewModel.latitude.value = latitude
            Log.d("LATITUDE****", latitude.toString())

            val longitude = data?.getDoubleExtra(LONGITUDE, 0.0)
            viewModel.longitude.value = longitude
            Log.d("LONGITUDE****", longitude.toString())

            val address = data?.getStringExtra(LOCATION_ADDRESS)
            viewModel.fullAddress.value = address
            Log.d("ADDRESS****", address.toString())
        }
    }

    fun showDateDialog(view: View) {
        val newCalendar = Calendar.getInstance()
        val dateFormatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)


        val datePickerDialog = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                val newDate = Calendar.getInstance()
                newDate.set(year, monthOfYear, dayOfMonth)

                viewModel.birthDate.value = dateFormatter.format(newDate.time)

            },
            newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH),
            newCalendar.get(Calendar.DAY_OF_MONTH)
        )

        datePickerDialog.show()
    }

    fun submit(view: View) {
        val token = session.getToken()!!
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.updateBusiness(token)
        }
    }
}
