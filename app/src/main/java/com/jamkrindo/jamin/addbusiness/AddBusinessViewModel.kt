package com.jamkrindo.jamin.addbusiness

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jamkrindo.jamin.common.model.business.UpdateBusinessRequest
import com.jamkrindo.jamin.common.model.business.repository.BusinessRepository
import com.jamkrindo.jamin.common.model.category.Category
import com.jamkrindo.jamin.common.model.category.repository.CategoryRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddBusinessViewModel : ViewModel() {

    var categories = MutableLiveData<List<Category>>()
    var businessName = MutableLiveData<String>()
    var selectedCategory = MutableLiveData<Category>()
    var selectedType = MutableLiveData<String>()

    var latitude = MutableLiveData<Double>()
    var longitude = MutableLiveData<Double>()
    var fullAddress = MutableLiveData<String>()

    var birthDate = MutableLiveData<String>()
    var income = MutableLiveData<String>()

    var pending = MutableLiveData<Boolean>()

    private val categoryRepository by lazy {
        CategoryRepository
    }

    private val businessRepository by lazy {
        BusinessRepository
    }

    suspend fun loadAvailableCategories() {
        val categories = categoryRepository.getCategories()

        GlobalScope.launch(Dispatchers.Main) {
            this@AddBusinessViewModel.categories.value = categories
        }
    }

    suspend fun updateBusiness(token: String) {
        GlobalScope.launch(Dispatchers.Main) {
            pending.value = true
        }
        val income = this.income.value!!.toDouble()
        val address = this.fullAddress.value
        val name = this.businessName.value

        val latitude = this.latitude.value
        val longitude = this.longitude.value

        val category = this.selectedCategory.value?.name
        val type = this.selectedType.value
        val birthDate = this.birthDate.value

        businessRepository.updateBusiness(token, UpdateBusinessRequest(
            name = name ?: "",
            businessForm = type ?: "INDIVIDUAL",
            address = address ?: "",
            category = category ?: "Restaurant",
            latitude = latitude ?: 0.0,
            longitude = longitude ?: 0.0,
            income = income,
            birthDate = birthDate ?: ""
        ))

        GlobalScope.launch(Dispatchers.Main) {
            pending.value = false
        }
    }
}