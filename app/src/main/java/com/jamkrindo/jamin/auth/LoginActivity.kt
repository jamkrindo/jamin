package com.jamkrindo.jamin.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import br.com.simplepass.loadingbutton.customViews.CircularProgressButton
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.session.Session
import com.jamkrindo.jamin.databinding.ActivityLoginBinding
import com.jamkrindo.jamin.main.MainActivity
import com.jamkrindo.jamin.verify.VerifyTokenActivity

class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var loginButton: CircularProgressButton
    private val session by lazy {
        Session(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        binding.lifecycleOwner = this
        binding.loginViewModel = viewModel

        checkIfLoggedIn()

        loginButton = findViewById(R.id.login_button)
        viewModel.loginSuccess.observe(this, Observer {
            if (it) {
                session.setPhoneNumber(viewModel.phoneNumber.value!!)

                val intent = Intent(this, VerifyTokenActivity::class.java)
                startActivity(intent)
            }
        })

        viewModel.pending.observe(this, Observer {
            if (it) {
                loginButton.startAnimation()
            } else {
                loginButton.revertAnimation()
            }
        })

        viewModel.loadFirebaseToken()
    }

    private fun checkIfLoggedIn() {
        if (session.getToken() != null) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}
