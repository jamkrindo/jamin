package com.jamkrindo.jamin.auth

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.iid.FirebaseInstanceId
import com.jamkrindo.jamin.common.model.auth.repository.AuthRepository
import com.jamkrindo.jamin.common.model.auth.repository.IAuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {

    private val tag by lazy {
        this::class.java.name
    }

    private val authRepository: IAuthRepository by lazy {
        AuthRepository
    }

    var phoneNumber = MutableLiveData<String>()
    var token = MutableLiveData<String>()
    var loginSuccess = MutableLiveData<Boolean>()
    var pending = MutableLiveData<Boolean>()

    fun onLoginButtonPress() {
        GlobalScope.launch(Dispatchers.IO) {
            login()
        }
    }

    private suspend fun login() {
        GlobalScope.launch(Dispatchers.Main) {
            pending.value = true
        }
        val success = authRepository.login(
            phoneNumber = phoneNumber.value ?: "",
            firebaseToken = token.value ?: ""
        )
        GlobalScope.launch(Dispatchers.Main) {
            pending.value = false
            loginSuccess.value = success
        }
    }

    fun loadFirebaseToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (!it.isSuccessful) {
                Log.w(tag, "getInstanceId failed", it.exception)
            }

            val token = it.result?.token
            Log.i(tag, token)

            this@LoginViewModel.token.value = token
        }
    }
}