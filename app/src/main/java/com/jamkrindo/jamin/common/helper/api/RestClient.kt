package com.jamkrindo.jamin.common.helper.api

import com.google.gson.Gson
import com.jamkrindo.jamin.common.model.auth.AuthService
import com.jamkrindo.jamin.common.model.business.BusinessService
import com.jamkrindo.jamin.common.model.category.CategoryService
import com.jamkrindo.jamin.common.model.credit.CreditService
import com.jamkrindo.jamin.common.model.form.FormService
import com.jamkrindo.jamin.common.model.news.NewsService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

object RestClient {
    private val retrofit by lazy {
        createRetrofit()
    }

    private fun createRetrofit(): Retrofit {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(logging)

        return Retrofit.Builder()
            .baseUrl("http://43.231.128.27:8000/")
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .client(httpClient.build())
            .build()
    }

    fun getCreditService(): CreditService {
        return retrofit.create(CreditService::class.java)
    }

    fun getAuthService(): AuthService {
        return retrofit.create(AuthService::class.java)
    }

    fun getFormService(): FormService {
        return retrofit.create(FormService::class.java)
    }

    fun getCategoryService(): CategoryService {
        return retrofit.create(CategoryService::class.java)
    }

    fun getBusinessService(): BusinessService {
        return retrofit.create(BusinessService::class.java)
    }

    fun getNewsService(): NewsService {
        return retrofit.create(NewsService::class.java)
    }
}