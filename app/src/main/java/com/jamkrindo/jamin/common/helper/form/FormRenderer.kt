package com.jamkrindo.jamin.common.helper.form

import android.content.Context
import android.text.InputType
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.helper.form.view.GeneratedEditText
import com.jamkrindo.jamin.common.helper.form.view.GeneratedMaterialSpinner
import com.jamkrindo.jamin.common.model.form.Form
import com.jaredrummler.materialspinner.MaterialSpinner
import com.jaredrummler.materialspinner.MaterialSpinnerAdapter

fun View.margin(left: Float? = null, top: Float? = null, right: Float? = null, bottom: Float? = null) {
    layoutParams<ViewGroup.MarginLayoutParams> {
        left?.run { leftMargin = dpToPx(this) }
        top?.run { topMargin = dpToPx(this) }
        right?.run { rightMargin = dpToPx(this) }
        bottom?.run { bottomMargin = dpToPx(this) }
    }
}

inline fun <reified T : ViewGroup.LayoutParams> View.layoutParams(block: T.() -> Unit) {
    if (layoutParams is T) block(layoutParams as T)
}

fun View.dpToPx(dp: Float): Int = context.dpToPx(dp)
fun Context.dpToPx(dp: Float): Int =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.displayMetrics).toInt()


class FormRenderer(private val context: Context) {

    fun renderFormObjects(json: List<Form>): ViewGroup {
        val formGroup = LinearLayout(context)
        formGroup.orientation = LinearLayout.VERTICAL
        json.forEach {
            val inputObject = it
            renderInputObject(inputObject, formGroup)
        }

        return formGroup
    }

    private fun renderInputObject(descriptor: Form, root: ViewGroup) {
        when (descriptor.type) {
            "select" -> renderRadioGroup(descriptor, root)
            "short" -> renderShortEditText(descriptor, root)
            "number" -> renderShortEditText(descriptor, root)
            "long" -> renderLongEditText(descriptor, root)
            else -> {
            }
        }
    }

    private fun renderRadioGroup(descriptor: Form, root: ViewGroup) {
        renderLabel(descriptor, root)

        val spinner = GeneratedMaterialSpinner(context)
        spinner.key = descriptor.id

        val optionsJson = descriptor.options

        val options = ArrayList<String>()
        optionsJson.forEach {
            options.add(it)
        }
        spinner.setAdapter(MaterialSpinnerAdapter(context, options))

        root.addView(spinner)
    }

    private fun renderShortEditText(descriptor: Form, root: ViewGroup) {
        renderLabel(descriptor, root)

        val input = GeneratedEditText(context)
        input.key = descriptor.id
        input.background = root.resources.getDrawable(R.drawable.rounded_text_input_background)
        input.margin(bottom = 18F)

        when (descriptor.type) {
            "short" -> input.inputType = InputType.TYPE_CLASS_TEXT
            "number" -> input.inputType = InputType.TYPE_CLASS_NUMBER
        }

        root.addView(input)
    }

    private fun renderLongEditText(descriptor: Form, root: ViewGroup) {
        renderLabel(descriptor, root)

        val input = GeneratedEditText(context)
        input.key = descriptor.id
        input.minLines = 5
        input.background = root.resources.getDrawable(R.drawable.rounded_text_input_background)
        input.gravity = Gravity.TOP
        input.margin(bottom = 18F)

        root.addView(input)
    }

    private fun dpToPixel(dp: Int): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    private fun renderLabel(descriptor: Form, root: ViewGroup) {
        val label = TextView(context)
        label.text = descriptor.label
        label.margin(bottom = 4F)

        root.addView(label)
    }
}
