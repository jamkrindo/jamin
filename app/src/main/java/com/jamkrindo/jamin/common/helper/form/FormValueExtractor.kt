package com.jamkrindo.jamin.common.helper.form

import android.view.ViewGroup
import androidx.core.view.children
import com.jamkrindo.jamin.common.helper.form.view.IGeneratedInput

object FormValueExtractor {

    fun extractValues(form: ViewGroup): List<String> {
        val values = ArrayList<String>()
        form.children.forEach {
            if (it is IGeneratedInput) {
                values.add(it.getInputValue())
            }
        }
        return values
    }
}