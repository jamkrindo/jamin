package com.jamkrindo.jamin.common.helper.form.view

import android.content.Context
import android.widget.EditText

class GeneratedEditText(context: Context) : EditText(context), IGeneratedInput {
    override fun getInputValue(): String = text.toString()
    var key: String = ""
}