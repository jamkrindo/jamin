package com.jamkrindo.jamin.common.helper.form.view

import android.content.Context
import com.jaredrummler.materialspinner.MaterialSpinner

class GeneratedMaterialSpinner(context: Context) : MaterialSpinner(context), IGeneratedInput {
    override fun getInputValue(): String {
        return this.getItems<String>()[this.selectedIndex]
    }


    var key: String = ""
}