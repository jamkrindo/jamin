package com.jamkrindo.jamin.common.helper.form.view

interface IGeneratedInput {
    fun getInputValue(): String
}