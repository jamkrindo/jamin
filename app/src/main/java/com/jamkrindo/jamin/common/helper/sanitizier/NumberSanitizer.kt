package com.jamkrindo.jamin.common.helper.sanitizier

import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

fun sanitizeMoney(amount: BigDecimal): String {
    val localeID = Locale("in", "ID")
    val format = NumberFormat.getCurrencyInstance(localeID)

    return format.format(amount)
}