package com.jamkrindo.jamin.common.model

import com.google.gson.annotations.SerializedName

class BaseResponse<T>(
    @SerializedName("data")
    val payload: T
)