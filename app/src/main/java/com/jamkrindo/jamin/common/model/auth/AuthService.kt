package com.jamkrindo.jamin.common.model.auth

import com.jamkrindo.jamin.common.model.user.GetUserResponse
import com.jamkrindo.jamin.common.model.user.User
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface AuthService {
    @POST("user/update-otp")
    suspend fun login(@Body request: LoginRequest): Response<LoginResponse>

    @POST("auth/login")
    suspend fun verifyToken(@Body request: VerifyTokenRequest): Response<VerifyTokenResponse>

    @GET("auth/check")
    suspend fun getUser(@Header("J-Auth") token: String): Response<GetUserResponse>
}