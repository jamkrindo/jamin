package com.jamkrindo.jamin.common.model.auth

data class LoginRequest (
    val phoneNum: String,
    val token: String
)

data class VerifyTokenRequest (
    val password: String,
    val username: String
)