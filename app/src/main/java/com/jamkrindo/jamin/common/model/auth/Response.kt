package com.jamkrindo.jamin.common.model.auth

data class LoginResponse(
    val success: Boolean
)

data class VerifyTokenResponse(
    val token: String
)