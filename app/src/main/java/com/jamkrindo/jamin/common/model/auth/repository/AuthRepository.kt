package com.jamkrindo.jamin.common.model.auth.repository

import com.jamkrindo.jamin.common.helper.api.RestClient
import com.jamkrindo.jamin.common.model.auth.LoginRequest
import com.jamkrindo.jamin.common.model.auth.VerifyTokenRequest
import com.jamkrindo.jamin.common.model.user.User
import java.lang.Exception

object AuthRepository : IAuthRepository {

    private val authService by lazy {
       RestClient.getAuthService()
    }

    override suspend fun getUser(token: String): User {
        val response = authService.getUser(token)
        if (!response.isSuccessful || response.body() == null) {
            throw Exception(response.message())
        }

        return response.body()!!.user
    }

    override suspend fun verifyToken(token: String, phoneNum: String): String? {
        val response = authService.verifyToken(VerifyTokenRequest(
            username = phoneNum,
            password = token
        ))
        if (!response.isSuccessful) {
            throw Exception(response.message())
        }

        return response.body()?.token
    }

    override suspend fun login(phoneNumber: String, firebaseToken: String): Boolean {
        val response = authService.login(LoginRequest(
            phoneNum = phoneNumber,
            token = firebaseToken
        ))
        if (!response.isSuccessful) {
            throw Exception(response.message())
        }

        return true
    }
}