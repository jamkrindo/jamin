package com.jamkrindo.jamin.common.model.auth.repository

import com.jamkrindo.jamin.common.model.user.User

interface IAuthRepository {
    suspend fun login(phoneNumber: String, firebaseToken: String) : Boolean
    suspend fun verifyToken(token: String, phoneNum: String) : String?
    suspend fun getUser(token: String): User
}