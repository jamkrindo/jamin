package com.jamkrindo.jamin.common.model.business

import com.jamkrindo.jamin.common.model.BaseResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface BusinessService {
    @POST("/business/add")
    suspend fun updateBusiness(
        @Header("J-Auth") token: String,
        @Body request: UpdateBusinessRequest
    ): Response<BaseResponse<UpdateBusinessResponse>>
}