package com.jamkrindo.jamin.common.model.business

data class UpdateBusinessRequest(
    val name: String,
    val category: String,
    val businessForm: String,
    val birthDate: String,
    val income: Double,
    val address: String,
    val latitude: Double,
    val longitude: Double
)