package com.jamkrindo.jamin.common.model.business

data class UpdateBusinessResponse(
    val success: String
)