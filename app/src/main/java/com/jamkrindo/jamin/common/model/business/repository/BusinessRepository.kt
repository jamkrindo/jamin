package com.jamkrindo.jamin.common.model.business.repository

import com.jamkrindo.jamin.common.helper.api.RestClient
import com.jamkrindo.jamin.common.model.business.UpdateBusinessRequest

object BusinessRepository {
    private val businessService by lazy {
        RestClient.getBusinessService()
    }

    suspend fun updateBusiness(token: String, request: UpdateBusinessRequest): Boolean {
        val response = businessService.updateBusiness(token, request)
        if (!response.isSuccessful || response.body() == null) {
            throw Exception(response.message())
        }

        return true
    }
}