package com.jamkrindo.jamin.common.model.category

data class Category(
    val id: Int,
    val name: String
) {
    override fun toString(): String {
        return name
    }
}