package com.jamkrindo.jamin.common.model.category

import com.jamkrindo.jamin.common.model.BaseResponse
import retrofit2.Response
import retrofit2.http.GET

interface CategoryService {
    @GET("business/category")
    suspend fun getCategories(): Response<BaseResponse<CategoriesResponse>>
}