package com.jamkrindo.jamin.common.model.category

data class CategoriesResponse(
    val categories: List<Category>
)