package com.jamkrindo.jamin.common.model.category.repository

import com.jamkrindo.jamin.common.helper.api.RestClient
import com.jamkrindo.jamin.common.model.category.Category

object CategoryRepository {
    private val categoryService by lazy {
        RestClient.getCategoryService()
    }

    suspend fun getCategories(): List<Category> {
        val response = categoryService.getCategories()
        if (!response.isSuccessful || response.body() == null) {
            throw Exception(response.message())
        }

        return response.body()!!.payload.categories
    }
}