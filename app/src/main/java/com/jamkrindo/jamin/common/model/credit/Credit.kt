package com.jamkrindo.jamin.common.model.credit

import java.io.Serializable
import java.math.BigDecimal

data class Credit(
    val id: String,
    val shortName: String,
    val longName: String,
    val description: String,
    val minLoan: BigDecimal,
    val maxLoan: BigDecimal,
    val interest: BigDecimal,
    val requirements: List<Requirements>,
    val bank: Bank
) : Serializable

data class Bank(
    val id: String,
    val code: String,
    val name: String,
    val imageURL: String
) : Serializable

data class Requirements(
    val id: String,
    val description: String
) : Serializable