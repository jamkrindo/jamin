package com.jamkrindo.jamin.common.model.credit

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface CreditService {
    @GET("credit/recommendation")
    suspend fun getCreditsRecommendation(
        @Header("J-Auth") authorization: String
    ): Response<BaseResponse<CreditsResponse>>
}