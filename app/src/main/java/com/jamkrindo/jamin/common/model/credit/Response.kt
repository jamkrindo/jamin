package com.jamkrindo.jamin.common.model.credit

import com.google.gson.annotations.SerializedName

data class BaseResponse<T> (
    @SerializedName("data")
    val payload: T
)

data class CreditsResponse(
    val credits: List<Credit>
)