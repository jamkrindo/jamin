package com.jamkrindo.jamin.common.model.credit.repository

import com.jamkrindo.jamin.common.helper.api.RestClient
import com.jamkrindo.jamin.common.model.credit.Credit
import java.lang.Exception

object CreditRepository : ICreditRepository {
    private val creditService by lazy {
        RestClient.getCreditService()
    }

    override suspend fun getCreditsRecommendation(token: String): List<Credit> {
        val response = creditService.getCreditsRecommendation(token)
        if (!response.isSuccessful) {
            throw Exception(response.message())
        }

        return response.body()?.payload?.credits ?: ArrayList()
    }
}
