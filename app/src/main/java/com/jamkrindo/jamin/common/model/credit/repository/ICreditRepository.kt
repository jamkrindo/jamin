package com.jamkrindo.jamin.common.model.credit.repository

import com.jamkrindo.jamin.common.model.credit.Credit

interface ICreditRepository {
    suspend fun getCreditsRecommendation(token: String): List<Credit>
}