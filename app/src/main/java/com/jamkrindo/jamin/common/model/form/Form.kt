package com.jamkrindo.jamin.common.model.form

import java.io.Serializable

data class Section (
    val payload: List<Form>
) : Serializable

data class Form (
    val id: String,
    val type: String,
    val label: String,
    var value: String,
    var options: List<String>
) : Serializable