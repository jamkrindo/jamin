package com.jamkrindo.jamin.common.model.form

import com.jamkrindo.jamin.common.model.BaseResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface FormService {
    @GET("/form/form-data")
    suspend fun getForm(@Header("J-Auth") authorization: String): Response<BaseResponse<FormResponse>>
}