package com.jamkrindo.jamin.common.model.form

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class FormDescriptor(
    @SerializedName("data")
    val sections: List<Section>
) : Serializable

data class FormResponse(
    @SerializedName("payload")
    val descriptor: FormDescriptor
)