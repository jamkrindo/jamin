package com.jamkrindo.jamin.common.model.form.repository

import com.jamkrindo.jamin.common.helper.api.RestClient
import com.jamkrindo.jamin.common.model.form.FormDescriptor

object FormRepository : IFormRepository {
    private val formService by lazy {
        RestClient.getFormService()
    }

    override suspend fun getForm(token: String): FormDescriptor {
        val response = formService.getForm(token)
        if (!response.isSuccessful || response.body() == null) {
            throw Exception(response.message())
        }

        return response.body()!!.payload.descriptor
    }
}