package com.jamkrindo.jamin.common.model.form.repository

import com.jamkrindo.jamin.common.model.form.FormDescriptor

interface IFormRepository {
    suspend fun getForm(token: String): FormDescriptor
}