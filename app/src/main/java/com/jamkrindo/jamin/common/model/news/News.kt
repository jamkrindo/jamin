package com.jamkrindo.jamin.common.model.news

import java.io.Serializable

data class News(
    val id: String,
    val title: String,
    val sector: String,
    val content: String
) : Serializable