package com.jamkrindo.jamin.common.model.news

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface NewsService {
    @GET("post")
    suspend fun getNews(
        @Header("J-Auth") authorization: String,
        @Query("sector") sector: String
    ): Response<BaseResponse<NewsResponse>>
}