package com.jamkrindo.jamin.common.model.news

import com.google.gson.annotations.SerializedName

data class BaseResponse<T> (
    @SerializedName("data")
    val payload: T
)

data class NewsResponse(
    val posts: List<News>
)