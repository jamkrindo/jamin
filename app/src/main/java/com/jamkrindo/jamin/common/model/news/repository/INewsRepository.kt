package com.jamkrindo.jamin.common.model.news.repository

import com.jamkrindo.jamin.common.model.news.News

interface INewsRepository {
    suspend fun getNews(token: String, sector: String): List<News>
}