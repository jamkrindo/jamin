package com.jamkrindo.jamin.common.model.news.repository

import com.jamkrindo.jamin.common.helper.api.RestClient
import com.jamkrindo.jamin.common.model.news.News
import java.lang.Exception

object NewsRepository : INewsRepository {
    private val newsService by lazy {
        RestClient.getNewsService()
    }

    override suspend fun getNews(token: String, sector: String): List<News> {
        val response = newsService.getNews(token, sector)
        if (!response.isSuccessful) {
            throw Exception(response.message())
        }

        return response.body()?.payload?.posts ?: ArrayList()
    }
}