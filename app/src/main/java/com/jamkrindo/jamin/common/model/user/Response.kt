package com.jamkrindo.jamin.common.model.user

data class GetUserResponse(
    val user: User
)