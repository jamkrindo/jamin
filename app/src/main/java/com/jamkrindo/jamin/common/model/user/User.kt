package com.jamkrindo.jamin.common.model.user

data class User(
    val id: String,
    val username: String,
    val email: String,
    val fullName: String,
    val phoneNumber: String,
    val admin: String,
    val ownBusiness: Boolean,
    val businessUpdated: String
)