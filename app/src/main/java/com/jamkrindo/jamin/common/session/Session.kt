package com.jamkrindo.jamin.common.session

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.core.content.edit

class Session(context: Context) {

    private var sharedPreferences: SharedPreferences
            = PreferenceManager.getDefaultSharedPreferences(context)

    fun setToken(token: String) {
        sharedPreferences
            .edit{
                putString("token", token)
                apply()
            }
    }

    fun getToken(): String? {
        return sharedPreferences.getString("token", null)
    }

    fun setPhoneNumber(phoneNumber: String) {
        sharedPreferences
            .edit{
                putString("phoneNumber", phoneNumber)
                apply()
            }
    }

    fun getPhoneNumber(): String? {
        return sharedPreferences.getString("phoneNumber", "")
    }
}