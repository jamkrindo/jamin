package com.jamkrindo.jamin.creditdetail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.helper.sanitizier.sanitizeMoney
import com.jamkrindo.jamin.common.model.credit.Credit
import com.jamkrindo.jamin.document.DocumentActivity

class CreditDetailActivity : AppCompatActivity() {

    private lateinit var requirementsView: ListView

    @BindView(R.id.credit_detail_bank_logo)
    lateinit var bankLogoView: ImageView

    @BindView(R.id.credit_detail_short_name)
    lateinit var shortNameView: TextView

    @BindView(R.id.credit_detail_description)
    lateinit var descriptionView: TextView

    @BindView(R.id.credit_detail_interest)
    lateinit var interestView: TextView

    @BindView(R.id.credit_detail_max)
    lateinit var maxView: TextView

    @BindView(R.id.proceed_button)
    lateinit var buttonView: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_credit_detail)

        ButterKnife.bind(this)

        val credit = intent!!.extras!!["credit"] as Credit
        val requirements = ArrayList<String>()

        credit.requirements.forEach {
            requirements.add(it.description)
        }

        val adapter = ArrayAdapter(this, R.layout.requirement_view_holder_layout, requirements)

        requirementsView = findViewById(R.id.credit_detail_requirements)
        requirementsView.adapter = adapter

        initializeView(credit)
    }

    private fun initializeView(credit: Credit) {
        Glide
            .with(this)
            .load(credit.bank.imageURL)
            .centerCrop()
            .into(bankLogoView)

        shortNameView.text = credit.shortName
        descriptionView.text = credit.description
        interestView.text = credit.interest.toString() + "%"
        maxView.text = sanitizeMoney(credit.maxLoan)
        buttonView.setOnClickListener {
            val intent = Intent(buttonView.context, DocumentActivity::class.java)
            buttonView.context.startActivity(intent)
        }
    }
}
