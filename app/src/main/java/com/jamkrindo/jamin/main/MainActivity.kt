package com.jamkrindo.jamin.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import butterknife.BindView
import butterknife.ButterKnife
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.addbusiness.AddBusinessActivity
import com.jamkrindo.jamin.addbusiness.AddBusinessViewModel
import com.jamkrindo.jamin.common.model.auth.repository.AuthRepository
import com.jamkrindo.jamin.common.session.Session
import com.jamkrindo.jamin.main.adapter.MainViewPagerAdapter
import com.jamkrindo.jamin.main.history.HistoryFragment
import com.jamkrindo.jamin.main.home.HomeFragment
import com.jamkrindo.jamin.main.profile.ProfileFragment
import com.jamkrindo.jamin.main.search.credit.CreditRecommendationsFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    @BindView(R.id.main_view_pager)
    lateinit var viewPager: ViewPager

    @BindView(R.id.nav_view)
    lateinit var navView: BottomNavigationView

    private val authRepository by lazy {
        AuthRepository
    }

    private val session by lazy {
        Session(this)
    }

    private val fragments = hashMapOf(
        0 to HomeFragment.newInstance(),
        1 to CreditRecommendationsFragment.newInstance(),
        2 to ProfileFragment.newInstance(),
        3 to HistoryFragment.newInstance()
    )

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                viewPager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                viewPager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                viewPager.currentItem = 2
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_test -> {
                viewPager.currentItem = 3
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.hide()


        ButterKnife.bind(this)
        setupViewPager(viewPager)

        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        GlobalScope.launch(Dispatchers.IO) {
            getUser(session.getToken()!!)
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val pagerAdapter = MainViewPagerAdapter(
            supportFragmentManager,
            fragments
        )
        viewPager.adapter = pagerAdapter
    }

    private suspend fun getUser(token: String) {
        val user = authRepository.getUser(token)
        if (!user.ownBusiness) {
            GlobalScope.launch(Dispatchers.Main) {
                val intent = Intent(this@MainActivity, AddBusinessActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
