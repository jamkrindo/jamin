package com.jamkrindo.jamin.main.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.jamkrindo.jamin.main.search.credit.CreditRecommendationsFragment

class MainViewPagerAdapter(
    fragmentManager: FragmentManager,
    private val fragments: HashMap<Int, Fragment>
) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragments[position] ?: CreditRecommendationsFragment.newInstance()
    }

    override fun getCount(): Int {
        return fragments.size
    }
}