package com.jamkrindo.jamin.main.history

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.addbusiness.AddBusinessActivity
import com.jamkrindo.jamin.submission.SubmissionActivity

class HistoryFragment : Fragment() {

    companion object {
        fun newInstance() = HistoryFragment()
    }

    private lateinit var viewModel: HistoryViewModel

    private lateinit var testButton: Button
    private lateinit var testButton1: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.history_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HistoryViewModel::class.java)
        // TODO: Use the ViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        testButton = view.findViewById(R.id.test_button)
        testButton1 = view.findViewById(R.id.test_button_1)

        testButton.setOnClickListener {
            val intent = Intent(requireContext(), SubmissionActivity::class.java)
            startActivity(intent)
        }

        testButton1.setOnClickListener {
            val intent = Intent(requireContext(), AddBusinessActivity::class.java)
            startActivity(intent)
        }
    }
}
