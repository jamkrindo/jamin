package com.jamkrindo.jamin.main.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.jamkrindo.jamin.common.model.news.News
import com.jamkrindo.jamin.common.model.news.repository.INewsRepository
import com.jamkrindo.jamin.common.model.news.repository.NewsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {
    private val newsRepository: INewsRepository by lazy {
        NewsRepository
    }

    val token = MutableLiveData<String>()
    val sector = MutableLiveData<String>()
    val news by lazy {
        MutableLiveData<List<News>>()
    }

    suspend fun loadNews() {
        val news = newsRepository.getNews(token.value!!, "Restoran")
        GlobalScope.launch(Dispatchers.Main) {
            this@HomeViewModel.news.value = news
        }
    }
}
