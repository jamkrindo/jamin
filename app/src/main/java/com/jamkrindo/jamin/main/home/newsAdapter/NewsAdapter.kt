package com.jamkrindo.jamin.main.home.newsAdapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.model.news.News

class NewsAdapter : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    private var news: List<News> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.news_view_holder_layout, parent, false)

        return NewsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return news.size
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val newsItem = news[position]
        holder.bind(newsItem)
    }

    fun addAll(news: List<News>) {
        this.news = news
    }

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var titleView: TextView = itemView.findViewById(R.id.news_title)
        var contentView: TextView = itemView.findViewById(R.id.news_content)

        @SuppressLint("SetTextI18n")
        fun bind(newsItem: News) {
            titleView.text = newsItem.title
            contentView.text = newsItem.content

            itemView.setOnClickListener {
                // TODO: Create news detail activity
            }
        }
    }
}