package com.jamkrindo.jamin.main.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.main.profile.adapter.ProfileDataAdapter
import com.jamkrindo.jamin.submission.SubmissionActivity
import kotlinx.android.synthetic.main.profile_fragment.*

class ProfileFragment : Fragment() {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    private lateinit var viewModel: ProfileViewModel

    private lateinit var testButton: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        profile_pager.adapter = ProfileDataAdapter(requireActivity().supportFragmentManager)
        profile_tablayout.setupWithViewPager(profile_pager)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        testButton = view.findViewById(R.id.test_button)
        testButton.setOnClickListener {
            val intent = Intent(requireContext(), SubmissionActivity::class.java)
            startActivity(intent)
        }
    }

}