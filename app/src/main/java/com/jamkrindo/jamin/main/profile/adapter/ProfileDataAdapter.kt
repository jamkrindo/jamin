package com.jamkrindo.jamin.main.profile.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.jamkrindo.jamin.main.profile.business.ProfileBusinessFragment
import com.jamkrindo.jamin.main.profile.personal.ProfilePersonalFragment

class ProfileDataAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val pages = listOf(
        ProfilePersonalFragment(),
        ProfileBusinessFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position) {
            0 -> "Data Pribadi"
            else -> "Data Usaha"
        }
    }

}