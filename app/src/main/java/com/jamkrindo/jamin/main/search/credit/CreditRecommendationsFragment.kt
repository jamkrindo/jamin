package com.jamkrindo.jamin.main.search.credit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.session.Session
import com.jamkrindo.jamin.main.search.credit.adapter.CreditsAdapter
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CreditRecommendationsFragment : Fragment() {

    companion object {
        fun newInstance() = CreditRecommendationsFragment()
    }

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(CreditRecommendationsViewModel::class.java)
    }

    private val session by lazy {
        Session(requireContext())
    }

    private lateinit var creditsRecycler: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.credit_recommendations_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val adapter = CreditsAdapter()
        creditsRecycler.layoutManager = LinearLayoutManager(
            requireContext(),
            RecyclerView.VERTICAL,
            false
        )
        creditsRecycler.adapter = adapter

        val token = session.getToken()!!
        viewModel.token.value = token

        GlobalScope.launch {
            viewModel.loadCredits()
        }

        viewModel.credits.observe(this, Observer {
            adapter.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        creditsRecycler = view.findViewById(R.id.credit_recommendation_recycler_view)
    }
}
