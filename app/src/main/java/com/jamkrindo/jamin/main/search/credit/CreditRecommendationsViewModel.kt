package com.jamkrindo.jamin.main.search.credit

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jamkrindo.jamin.common.model.credit.Credit
import com.jamkrindo.jamin.common.model.credit.repository.CreditRepository
import com.jamkrindo.jamin.common.model.credit.repository.ICreditRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CreditRecommendationsViewModel : ViewModel() {
    private val creditsRepository: ICreditRepository by lazy {
        CreditRepository
    }

    val token = MutableLiveData<String>()
    val credits by lazy {
        MutableLiveData<List<Credit>>()
    }

    suspend fun loadCredits() {
        val credits = creditsRepository.getCreditsRecommendation(token.value!!)
        GlobalScope.launch(Dispatchers.Main) {
            this@CreditRecommendationsViewModel.credits.value = credits
        }
    }
}
