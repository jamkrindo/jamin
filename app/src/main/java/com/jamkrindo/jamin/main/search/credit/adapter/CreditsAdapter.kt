package com.jamkrindo.jamin.main.search.credit.adapter

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.helper.sanitizier.sanitizeMoney
import com.jamkrindo.jamin.common.model.credit.Credit
import com.jamkrindo.jamin.creditdetail.CreditDetailActivity
import javax.annotation.Nullable

class CreditsAdapter : RecyclerView.Adapter<CreditsAdapter.CreditViewHolder>() {
    private var credits: List<Credit> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.credit_view_holder_layout, parent, false)

        return CreditViewHolder(view)
    }

    override fun getItemCount(): Int {
        return credits.size
    }

    override fun onBindViewHolder(holder: CreditViewHolder, position: Int) {
        val credit = credits[position]
        holder.bind(credit)
    }

    fun addAll(credits: List<Credit>) {
        this.credits = credits
    }

    class CreditViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var imageView: ImageView = itemView.findViewById(R.id.credit_bank_logo)
        var nameView: TextView = itemView.findViewById(R.id.credit_short_name)
        var descriptionView: TextView = itemView.findViewById(R.id.credit_description)
        var interestView: TextView = itemView.findViewById(R.id.credit_interest)
        var maxLoanView: TextView = itemView.findViewById(R.id.credit_max_loan)

        @SuppressLint("SetTextI18n")
        fun bind(credit: Credit) {
            Glide
                .with(itemView)
                .load(credit.bank.imageURL)
                .centerCrop()
                .into(imageView)

            nameView.text = credit.shortName
            descriptionView.text = credit.description
            interestView.text = credit.interest.toString() + "%"
            maxLoanView.text = sanitizeMoney(credit.maxLoan)

            itemView.setOnClickListener {
                val intent = Intent(itemView.context, CreditDetailActivity::class.java)
                intent.putExtra("credit", credit)

                itemView.context.startActivity(intent)
            }
        }
    }
}