package com.jamkrindo.jamin.simplebusiness

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jamkrindo.jamin.R

class SimpleBusinessInfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_business_info)
    }
}
