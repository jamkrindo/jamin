package com.jamkrindo.jamin.submission

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.model.form.FormDescriptor
import com.jamkrindo.jamin.common.session.Session
import com.jamkrindo.jamin.submission.adapter.SectionAdapter
import com.jamkrindo.jamin.submission.section.business.BusinessInfoFragment
import com.jamkrindo.jamin.submission.section.financial.FinancialInfoFragment
import com.jamkrindo.jamin.submission.section.legal.LegalInfoFragment
import com.jamkrindo.jamin.submission.section.personal.PersonalInfoFragment
import com.rd.PageIndicatorView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SubmissionActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    private lateinit var pageIndicator: PageIndicatorView
    private lateinit var progressBar: ProgressBar

    private lateinit var viewModel: SubmissionViewModel

    private val session by lazy {
        Session(this)
    }

    private lateinit var fragments: HashMap<Int, Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submission)

        supportActionBar?.hide()

        viewModel = ViewModelProviders.of(this).get(SubmissionViewModel::class.java)

        viewPager = findViewById(R.id.submission_view_pager)
        pageIndicator = findViewById(R.id.submission_page_indicator)
        progressBar = findViewById(R.id.submission_progress_bar)


        viewModel.formDescriptor.observe(this, Observer {
            Log.i(SubmissionActivity::class.java.name, it.sections[0].payload[0].id)
            initializeForm(it)
        })

        viewModel.currentItem.observe(this, Observer {
            if (it == 5) {

            }
        })

        val token = session.getToken()!!
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.loadForm(token)
        }
    }

    private fun initializeForm(descriptor: FormDescriptor) {
        this.fragments = hashMapOf(
            0 to PersonalInfoFragment.newInstance(descriptor),
            1 to BusinessInfoFragment.newInstance(descriptor),
            2 to LegalInfoFragment.newInstance(descriptor),
            3 to FinancialInfoFragment.newInstance(descriptor)
        )
        viewPager.adapter = SectionAdapter(supportFragmentManager, fragments)
        viewPager.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }
}
