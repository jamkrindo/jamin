package com.jamkrindo.jamin.submission

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jamkrindo.jamin.common.model.form.FormDescriptor
import com.jamkrindo.jamin.common.model.form.repository.FormRepository
import com.jamkrindo.jamin.common.model.form.repository.IFormRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SubmissionViewModel : ViewModel() {

    val currentItem = MutableLiveData<Int>()
    val formDescriptor = MutableLiveData<FormDescriptor>()

    private val formRepository: IFormRepository by lazy {
        FormRepository
    }

    suspend fun loadForm(token: String) {
        val formDescriptor = formRepository.getForm(token)
        GlobalScope.launch(Dispatchers.Main) {
            this@SubmissionViewModel.formDescriptor.value = formDescriptor
        }
    }

    fun propagateChange(sectionId: Int, value: List<String>): Boolean {
        var index = 0
        formDescriptor.value!!.sections[sectionId].payload.forEach {
            it.value = value[index]
            index += 1
        }
        currentItem.value = sectionId + 1

        return true
    }
}