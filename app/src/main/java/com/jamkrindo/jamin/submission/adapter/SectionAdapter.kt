package com.jamkrindo.jamin.submission.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.jamkrindo.jamin.submission.section.personal.PersonalInfoFragment

class SectionAdapter(fragmentManager: FragmentManager, private val fragments: HashMap<Int, Fragment>) :
    FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return fragments[position]!!
    }

    override fun getCount(): Int {
        return fragments.size
    }
}