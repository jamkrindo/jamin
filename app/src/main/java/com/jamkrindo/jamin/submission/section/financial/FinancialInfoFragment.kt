package com.jamkrindo.jamin.submission.section.financial

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout

import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.helper.form.FormRenderer
import com.jamkrindo.jamin.common.helper.form.FormValueExtractor
import com.jamkrindo.jamin.common.model.form.FormDescriptor
import com.jamkrindo.jamin.submission.SubmissionViewModel

class FinancialInfoFragment : Fragment() {

    companion object {
        fun newInstance(descriptor: FormDescriptor): FinancialInfoFragment {
            val bundle = Bundle()
            bundle.putSerializable("descriptor", descriptor)

            val fragment = FinancialInfoFragment()
            fragment.arguments = bundle

            return fragment
        }
    }

    private lateinit var viewModel: FinancialInfoViewModel
    private lateinit var activityViewModel: SubmissionViewModel
    private lateinit var formContainer: FrameLayout
    private lateinit var formRenderer: FormRenderer
    private lateinit var button: Button
    private lateinit var formDescriptor: FormDescriptor

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.financial_info_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FinancialInfoViewModel::class.java)
        // TODO: Use the ViewModel
        formDescriptor = arguments?.get("descriptor") as FormDescriptor
        activityViewModel = ViewModelProviders.of(requireActivity()).get(SubmissionViewModel::class.java)

        formRenderer = FormRenderer(requireContext())

        val formGroup = formRenderer.renderFormObjects(formDescriptor.sections[0].payload)
        formContainer.addView(formGroup)

        button.setOnClickListener {
            activityViewModel.propagateChange(1, FormValueExtractor.extractValues(formContainer))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        formContainer = view.findViewById(R.id.financial_info_form_container)
        button = view.findViewById(R.id.financial_info_next)
    }
}
