package com.jamkrindo.jamin.verify

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.goodiebag.pinview.Pinview
import com.jamkrindo.jamin.R
import com.jamkrindo.jamin.common.session.Session
import com.jamkrindo.jamin.main.MainActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class VerifyTokenActivity : AppCompatActivity() {

    private lateinit var pinView: Pinview
    private lateinit var viewModel: VerifyTokenViewModel
    private val session by lazy {
        Session(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_token)
        supportActionBar?.hide()

        viewModel = ViewModelProviders.of(this).get(VerifyTokenViewModel::class.java)

        pinView = findViewById(R.id.verify_token_pin_view)
        pinView.setPinViewEventListener { pinview, _ ->
            submitOtp(pinview.value)
        }

        viewModel.token.observe(this, Observer {
            session.setToken(it)

            val intent = Intent(this@VerifyTokenActivity, MainActivity::class.java)
            startActivity(intent)
        })
    }

    private fun submitOtp(otp: String) {
        val phoneNumber = session.getPhoneNumber()!!
        GlobalScope.launch(Dispatchers.IO) {
            viewModel.submitOtp(otp, phoneNumber)
        }
    }
}
