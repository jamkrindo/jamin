package com.jamkrindo.jamin.verify

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jamkrindo.jamin.common.model.auth.repository.AuthRepository
import com.jamkrindo.jamin.common.model.auth.repository.IAuthRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class VerifyTokenViewModel : ViewModel() {

    private val authService: IAuthRepository by lazy {
        AuthRepository
    }

    val token = MutableLiveData<String>()

    suspend fun submitOtp(otp: String, phoneNumber: String) {
        val response = authService.verifyToken(
            token = otp,
            phoneNum = phoneNumber
        )
        GlobalScope.launch(Dispatchers.Main) {
            token.value = response
        }
    }
}